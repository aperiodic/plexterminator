env: env/deps-installed.tgt
env/deps-installed.tgt: requirements.txt dev-requirements.txt
	python -m venv env
	./env/bin/pip install -r requirements.txt -r dev-requirements.txt
	./env/bin/pip install -e .
	@touch env/deps-installed.tgt
