import warnings
from functools import reduce

from plexapi.media import Media
from plexapi.video import Episode, Season, Show

from plexterminator.type_definitions import GigaBytes, MediaError


BITS_PER_BYTE = 8
KILOBITS_PER_GIGABIT = 1_000_000
MILLISECONDS_PER_SECOND = 1_000


def size_of_media(media: Media) -> GigaBytes:
  if media.duration is None:
    raise MediaError(f"Media {media} has no duration")
  if media.bitrate is None:
    raise MediaError(f"Media {media} has no bitrate!")

  duration_in_seconds = media.duration / MILLISECONDS_PER_SECOND
  bitrate_in_kbps = media.bitrate
  size_in_kbps = duration_in_seconds * bitrate_in_kbps
  return size_in_kbps / (BITS_PER_BYTE * KILOBITS_PER_GIGABIT)


def size_of_episode(episode: Episode) -> GigaBytes:
  def add_size_of_media_or_warn_on_error(total_size: GigaBytes, media) -> GigaBytes:
    try:
      return total_size + size_of_media(media)
    except MediaError:
      warnings.warn(
        f"Size of media for episode '{episode.show().title} - {episode.title}' could"
        f" not be computed"
      )
      return total_size

  return reduce(add_size_of_media_or_warn_on_error, episode.media, 0)


def size_of_season(season: Season) -> GigaBytes:
  return sum(map(size_of_episode, season.episodes()))

def size_of_show(show: Show) -> GigaBytes:
  return sum(map(size_of_season, show.seasons()))
