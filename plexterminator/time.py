import re
from datetime import datetime

FRACTIONAL_SECONDS = re.compile("\.\d+")


def iso_without_fractional_seconds(iso_dt: str) -> str:
  return FRACTIONAL_SECONDS.sub("", iso_dt)


def expand_zulu(iso_dt: str) -> str:
  return iso_dt.replace('Z', '+00:00')


def parse_iso_zulu_datetime(iso_dt: str) -> datetime:
  no_frac_seconds = iso_without_fractional_seconds(iso_dt)
  zulu_expanded = expand_zulu(no_frac_seconds)
  return datetime.fromisoformat(zulu_expanded)
