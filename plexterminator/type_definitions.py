from datetime import datetime
from enum import Enum
from numbers import Real
from pathlib import Path
from typing import Any, Dict, NamedTuple, NewType, Union

from requests import Response


## Atomic Types
## ============

GigaBytes = NewType('GigaBytes', int)
Seconds = NewType('Seconds', int)
UnixTimestamp = NewType('UnixTimestamp', int)

class ShowNormalization(Enum):
  NONE = 1
  SEASON = 2
  ALL = 3


## Composite Types
## ===============

QBTorrent = NewType('QBTorrent', Dict[str, Any])

class Torrent(NamedTuple):
  hash: str
  filename_stem: str
  category: str
  size: int
  age: Seconds
  finished_at: UnixTimestamp
  ratio: float

_MOVIE_SUFFIXES = {
  '.avi',
  '.m2v',
  '.m4p',
  '.m4v',
  '.mkv',
  '.mov',
  '.mp2',
  '.mp4',
  '.mpeg',
  '.mpg',
  '.mpv',
  '.webm',
}

def _qb_torrent_name_to_filename_stem(name: str) -> str:
  p = Path(name)
  if p.suffix in _MOVIE_SUFFIXES:
    return p.stem
  else:
    return p.name

def qb_torrent_to_torrent(qb: QBTorrent) -> Torrent:
  return Torrent(
    hash = qb['hash'],
    filename_stem = _qb_torrent_name_to_filename_stem(qb['name']),
    category = qb['category'],
    size = qb['size'],
    age = qb['time_active'],
    finished_at = qb['completion_on'],
    ratio = qb['ratio'],
  )


class RadarrConfig(NamedTuple):
  api_url: str
  api_key: str

class SizeAndViews(NamedTuple):
  size: GigaBytes
  views: Real

class TautulliConfig(NamedTuple):
  api_url: str
  api_key: str

class MovieBasic(NamedTuple):
  title: str
  year: int
  rating_key: int
  radarr_id: int
  library_path: str

class MovieDownload(NamedTuple):
  title: str
  year: int
  rating_key: int
  radarr_id: int
  library_path: str
  torrent_path: str
  added_at: datetime


class MovieFull(NamedTuple):
  title: str
  year: int
  rating_key: int
  radarr_id: int
  qb_hash: str
  library_path: str
  torrent_path: str
  added_at: datetime
  size: int

  @classmethod
  def from_match(_klass, download: MovieDownload, torrent: Torrent):
    return _klass(
      **download._asdict(),
      qb_hash = torrent.hash,
      size = torrent.size,
    )


def torrent_filename_stem(movie: Union[MovieDownload, MovieFull]) -> str:
  path = Path(movie.torrent_path)
  return path.stem



RadarrHistoryEvent = NewType('RadarrHistoryEvent', Dict[str, Any])
RadarrDownload = NewType('RadarrDownload', Dict[str, Any])


## Errors
## ======

class MediaError(ValueError):
  pass

class HistoryEmpty(ValueError):
  pass

class HttpError(RuntimeError):
  def __init__(self, message: str, response: Response):
    self.response = response
    super().__init__(message)
