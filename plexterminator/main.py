import sys
import logging
from configparser import ConfigParser

from tabulate import tabulate

import plexterminator.annotate as annotate
import plexterminator.survey as survey
import plexterminator.plex as plex
from plexterminator.type_definitions import TautulliConfig


def parse_config(path: str) -> ConfigParser:
  cfg = ConfigParser()
  cfg.read(path)
  return cfg


def configure_logger(logger: logging.Logger) -> None:
  stdout = logging.StreamHandler()
  stdout.setLevel(logging.INFO)
  logger.addHandler(stdout)


def main(config_path: str, logger: logging.Logger, *args):
  cfg = parse_config(config_path)
  tautulli_cfg = TautulliConfig(
    api_url = cfg['tautulli']['api_url'], api_key = cfg['tautulli']['api_key'],
  )

  logger.info("Logging in to Plex...")
  user = plex.account(cfg['plex']['user'], cfg['plex']['password'])

  server = cfg['plex']['server']
  logger.info(f"Connecting to server '{server}'...")
  server = plex.server(user, server)
  tv_library = server.library.section('TV Shows')

  logger.info("Calculating show sizes and view counts (this might take a while)...")
  title_to_size_and_views = survey.show_sizes_and_view_counts(tautulli_cfg, tv_library)
  with_marked_titles = annotate.mark_missing_shows(title_to_size_and_views)

  title_sizes_and_views = [
    [title, size_and_views.size, max(size_and_views.views, 0)]
    for title, size_and_views
    in with_marked_titles.items()
  ]
  logger.info("All done!\n")

  sizes_and_views_table = tabulate(
    title_sizes_and_views,
    headers=['Title', 'Size (GB)', 'Views (Seasons)'],
    floatfmt='.2f',
  )
  print(sizes_and_views_table)


if __name__ == "__main__":
  if len(sys.argv) == 1:
    print("Error: no path to config file given", file=sys.stderr)
    print(f"Usage: {sys.argv[0]} config.cfg", file=sys.stderr)
    sys.exit(1)
  else:
    logger = logging.getLogger('plexterminator')
    logger.setLevel(logging.INFO)
    configure_logger(logger)
    main(sys.argv[1], logger)
