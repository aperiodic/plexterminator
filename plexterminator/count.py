""" This module is for counting how many times Plexs shows or movies have been
watched. It's named in juxtaposition to the `plexterminator.size` module, though
the name `count` is a bit more ambiguous.

This module is mostly an abstraction over the raw Tautulli API; its functions are
defined in terms of information the rest of the codebase cares about (e.g. the total
view count for a show), and they know which Tautulli API method to call and where to
look in the response to get that information.
"""

from numbers import Real

from plexapi.video import Show

import plexterminator.tautulli as taut
from .type_definitions import HttpError, ShowNormalization, TautulliConfig


def seasons(show: Show) -> int:
  return len(show.seasons())

def total_episodes(show: Show) -> int:
  return sum(map(lambda season: len(season.episodes()), show.seasons()))

def average_season_length(show: Show):
  return total_episodes(show) / seasons(show)

def show_views(
  cfg: TautulliConfig,
  show: Show,
  normalization = ShowNormalization.SEASON
) -> Real:
  """ The `normaliation` argument defines how to normalize the show's overall episode view
  count; it must be a member of the `ShowNormalization` enum defined in
  `plexterminator.type_definitions`:
    - `NONE`: no normalization, count is number of times an episode has been watched;
    - `SEASON`: normalize by average season length; count is number of seasons watched;
    - `ALL`: normalize by all episodes; count is number of times show has been watched.

  The default normalization value is `SEASON`. Normalization is recommended because it
  puts old shows and cartoons with 20+ episodes a season on equal footing with modern
  live-action shows that have 10-14 (or fewer) episodes a season"""

  try:
    view_history = taut.show_view_history(cfg, show.ratingKey)
  except HttpError as e:
    if e.response.status_code == 404:
      return -1
    else:
      raise e

  count = view_history["recordsFiltered"]
  if normalization == ShowNormalization.NONE:
    return count
  elif normalization == ShowNormalization.ALL:
    return count / len(show.episodes())
  elif normalization == ShowNormalization.SEASON:
    return count / average_season_length(show)
  else:
    raise ValueError(f"Don't know how to normalize by {normalization}")
