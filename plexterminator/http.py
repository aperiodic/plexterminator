from typing import Any, Dict

import requests

from .type_definitions import HttpError


def get_or_raise(url: str, params: Dict[str, Any]):
  resp = requests.get(url, params=params)
  if resp.status_code >= 400:
    raise HttpError(
      f"Radarr responded with a {resp.status_code} to request for {resp.url}", resp
    )
  else:
    return resp
