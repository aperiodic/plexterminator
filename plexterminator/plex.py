from plexapi.myplex import MyPlexAccount
from plexapi.server import PlexServer


def account(username: str, password: str) -> MyPlexAccount:
  return MyPlexAccount(username, password)

def server(account: MyPlexAccount, name: str) -> PlexServer:
  return account.resource(name).connect()
