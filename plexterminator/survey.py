import logging
from typing import Any, Dict, Iterable, List, Optional

from plexapi.library import Library
from plexapi.video import Movie

import plexterminator.count as count
import plexterminator.radarr as radarr
import plexterminator.qbittorrent as qb
from plexterminator.plex_helpers import movie_directory
from plexterminator.size import size_of_show
from plexterminator.type_definitions import (
  GigaBytes,
  HistoryEmpty,
  MovieBasic,
  MovieDownload,
  MovieFull,
  RadarrConfig,
  ShowNormalization,
  SizeAndViews,
  TautulliConfig,
  Torrent,
  torrent_filename_stem,
)


_logger = logging.getLogger(__name__)


def show_sizes(tv_library: Library) -> Dict[str, GigaBytes]:
  shows_and_sizes = [(show.title, size_of_show(show)) for show in tv_library.all()]
  shows_by_size = sorted(
    shows_and_sizes,
    key = lambda show_and_size: show_and_size[1],
    reverse = True,
  )
  return dict(shows_by_size)


def show_view_counts(
  tautulli_cfg: TautulliConfig,
  tv_library: Library,
  normalization = ShowNormalization.SEASON,
) -> Dict[str, int]:
  shows_and_counts = [
    (show.title, count.show_views(tautulli_cfg, show, normalization))
    for show in tv_library.all()
  ]
  shows_by_count = sorted(
    shows_and_counts,
    key = lambda show_and_count: show_and_count[1],
    reverse = True,
  )
  return dict(shows_by_count)


def show_sizes_and_view_counts(
  tautulli_cfg: TautulliConfig,
  tv_library: Library,
  normalization = ShowNormalization.SEASON,
) -> Dict[str, SizeAndViews]:
  sizes = show_sizes(tv_library)
  views = show_view_counts(tautulli_cfg, tv_library, normalization)

  return {
    title: SizeAndViews(size = size, views = views[title])
    for title, size in sizes.items()
  }


def _combine_movie_identifiers(plex: Movie, radarr: Dict[str, Any]) -> MovieBasic:
  return MovieBasic(
    title = radarr['title'],
    year = radarr['year'],
    rating_key = plex.ratingKey,
    radarr_id = radarr['id'],
    library_path = radarr['path'],
  )


def movies_basic(movie_library: Library, radarr_cfg: RadarrConfig) -> List[MovieBasic]:
  plex_movies = {str(movie_directory(m)): m for m in movie_library.all()}
  return [
    _combine_movie_identifiers(plex_movies[r_movie['path']], r_movie)
    for r_movie in radarr.all_movies(radarr_cfg)
    if r_movie['path'] in plex_movies
  ]


def _get_download_info(
  radarr_cfg: RadarrConfig, movie: MovieBasic,
) -> Optional[MovieDownload]:
  download = radarr.movie_download(radarr_cfg, movie.radarr_id)

  if download is None:
    _logger.warning(
      f"Couldn't find download for {movie.title} ({movie.year}) [radarr id:"
      f" {movie.radarr_id}]"
    )
    return None
  else:
    return MovieDownload(
      title=movie.title,
      year=movie.year,
      rating_key=movie.rating_key,
      radarr_id=movie.radarr_id,
      library_path=movie.library_path,
      torrent_path=download["data"]["droppedPath"],
      added_at=download["movie"]["added"],
    )


def movies_downloaded(
  movie_library: Library, radarr_cfg: RadarrConfig,
) -> List[MovieDownload]:
  mov_ids = movies_basic(movie_library, radarr_cfg)
  return list(
    filter(bool, [_get_download_info(radarr_cfg, m_id) for m_id in mov_ids])
  )


def _match_movies_with_qb_torrents(
  downloads: Iterable[MovieDownload], qb_torrents: Iterable[Torrent],
) -> Dict[str,Any]:
  movies_by_filename_stem = {torrent_filename_stem(m).lower(): m for m in downloads}
  torrent_filename_stems = set(map(lambda qbt: qbt.filename_stem.lower(), qb_torrents))

  matches = {
    qbt: movies_by_filename_stem[qbt.filename_stem.lower()]
    for qbt in qb_torrents
    if qbt.filename_stem.lower() in movies_by_filename_stem
  }

  unmatched_movies = [m for m in downloads if torrent_filename_stem(m).lower() not in torrent_filename_stems]
  unmatched_torrents = [t for t in qb_torrents if t.filename_stem.lower() not in movies_by_filename_stem]

  print(f"exact matching found {len(matches)}, but {len(unmatched_movies)} movies and {len(unmatched_torrents)} torrents are left over")

  for ut in unmatched_torrents:
    stem_lowered = ut.filename_stem.lower()
    for um in unmatched_movies:
      if stem_lowered in um.torrent_path.lower():
        print(f"quadratic search found match between {ut.filename_stem} and {um.torrent_path}")
        matches[ut] = um
        unmatched_movies.remove(um)
        unmatched_torrents.remove(ut)
        break

  print(f"found {len(matches)} matches between {len(qb_torrents)} torrents and {len(downloads)} movies, left with {len(unmatched_torrents)} unmatched torrents and {len(unmatched_movies)} unmatched movies")

  return {
    'matches': matches,
    'unmatched_movies': unmatched_movies,
    'unmatched_torrents': unmatched_torrents,
  }


def _add_qb_info(
   downloads: Iterable[MovieDownload], qb_torrents: Iterable[Torrent],
) -> List[MovieFull]:
  matches = _match_movies_with_qb_torrents(downloads, qb_torrents)['matches']
  return [
    MovieFull.from_match(movie_download, torrent)
    for torrent, movie_download in matches.items()
  ]


def movies_full(
  movie_library: Library, radarr_cfg: RadarrConfig, qb_client,
) -> List[MovieFull]:
  downloaded = movies_downloaded(movie_library, radarr_cfg)
  qb_torrents = qb.get_movie_torrents(qb_client)
  return _add_qb_info(downloaded, qb_torrents)
