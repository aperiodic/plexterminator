""" Some simple functions for working with plexapi objects."""

import warnings
from pathlib import Path

from plexapi.video import Movie


def movie_location(movie: Movie) -> Path:
  if len(movie.locations) > 1:
    warnings.warn(f"Movie '{movie.title}' has multiple locations")
  return Path(movie.locations[0])


def movie_directory(movie: Movie) -> Path:
  return movie_location(movie).parent
