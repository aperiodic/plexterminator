import json
import logging
import warnings
from datetime import datetime
from functools import reduce
from pathlib import Path
from typing import Any, Dict, List, Optional

import requests

from .http import get_or_raise
from .time import parse_iso_zulu_datetime
from .type_definitions import (
  HistoryEmpty,
  RadarrConfig,
  RadarrDownload,
  RadarrHistoryEvent,
)


_logger = logging.getLogger(__name__)


def _cfg_params(cfg: RadarrConfig) -> Dict[str, str]:
  return {
    'apikey': cfg.api_key
  }


def history_params(
  page: int = 1, page_size: int = 100, sort_key: str = "date", sort_dir: str = "desc",
) -> Dict[str, Any]:
  return {
    "page": page,
    "pageSize": page_size,
    "sortKey": sort_key,
    "sortDir": sort_dir,
  }


def movies_endpoint(cfg: RadarrConfig) -> str:
  return f"{cfg.api_url}/movie"


def history_endpoint(cfg: RadarrConfig) -> str:
  return f"{cfg.api_url}/history"


def all_movies(cfg: RadarrConfig) -> List[Dict[str, Any]]:
  resp = get_or_raise(movies_endpoint(cfg), _cfg_params(cfg))
  return json.loads(resp.text)


def _parse_date_in_field(
  record: Dict[str, Any], field: List[str]
) -> Dict[str, Any]:
  # iteration var for field loop, not necessarily record to return
  rec = record
  for f in field:
    rec_has_field_segment = f in rec
    field_is_dict = isinstance(rec[f], dict) if rec_has_field_segment else False
    field_has_subfield = f in rec[f] if field_is_dict else False

    if rec_has_field_segment and field_is_dict:
      # step to the next subfield
      rec = rec[f]
    elif rec_has_field_segment:
      rec[f] = parse_iso_zulu_datetime(rec[f])

  return record


_HISTORY_RECORD_DATE_FIELDS = [
  ['date'],
  ['movie', 'inCinemas'],
  ['movie', 'physicalRelease'],
  ['movie', 'lastInfoSync'],
  ['movie', 'added'],
]

def _parse_history_record(record: Dict[str, Any]) -> RadarrHistoryEvent:
  return reduce(_parse_date_in_field, _HISTORY_RECORD_DATE_FIELDS, record)


def movie_history(cfg: RadarrConfig, movie_id: int) -> List[RadarrHistoryEvent]:
  params = {
    "movieId": movie_id,
    **history_params(),
    **_cfg_params(cfg),
  }

  resp = get_or_raise(history_endpoint(cfg), params)
  data = json.loads(resp.text)
  records = data["records"]

  if len(records) == 0:
    raise HistoryEmpty(
      f"Could not find radarr history for movie with ID {movie_id}"
    )

  total_records = data["totalRecords"]
  page_size = data["pageSize"]
  if total_records > page_size:
    warnings.warn(
      f"Missing history data: {total_records} entries exist but only {page_size} were"
      f" retrieved for radarr id {movie_id}"
    )

  sorted_records = sorted(records, key = lambda r: r["date"], reverse = True)
  return [_parse_history_record(r) for r in sorted_records]


def movie_download(cfg: RadarrConfig, movie_id: int) -> Optional[RadarrDownload]:
  history_events = movie_history(cfg, movie_id)

  if history_events[0]["eventType"] == "downloadFolderImported":
    return history_events[0]
  else:
    _logger.info(
      f"No download info for movie with radarr ID {movie_id}; is it deleted or still"
      f" being downloaded?"
    )
    return None
