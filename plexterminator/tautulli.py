import json
from typing import Any, Dict

import requests

from .http import get_or_raise
from .type_definitions import TautulliConfig


def _cfg_params(cfg: TautulliConfig) -> Dict[str, str]:
  return {
    'apikey': cfg.api_key
  }


def show_view_history(cfg: TautulliConfig, show_rating_key: int) -> Dict[str, Any]:
  params = {
    'cmd': 'get_history',
    'grandparent_rating_key': show_rating_key,
    **_cfg_params(cfg),
  }

  resp = get_or_raise(cfg.api_url, params)
  return json.loads(resp.text)["response"]["data"]


def movie_view_history(cfg: TautulliConfig, movie_rating_key: int) -> Dict[str, Any]:
  params = {
    'cmd': 'get_history',
    'rating_key': movie_rating_key,
    **_cfg_params(cfg),
  }

  resp = get_or_raise(cfg.api_url, params)
  return json.loads(resp.text)["response"]["data"]
