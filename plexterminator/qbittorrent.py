from pathlib import Path
from typing import Any, Dict, Iterable, List

from plexterminator.type_definitions import QBTorrent, Torrent, qb_torrent_to_torrent


def is_movie_torrent(torrent: QBTorrent) -> bool:
  return torrent['category'] == 'Movies'

def filter_movie_torrents(torrents: Iterable[QBTorrent]) -> List[QBTorrent]:
  return [t for t in torrents if is_movie_torrent(t)]

def get_movie_torrents(qb_client) -> List[Torrent]:
  return [qb_torrent_to_torrent(t) for t in filter_movie_torrents(client.torrents())]
