from typing import Dict

from plexterminator.type_definitions import SizeAndViews


def mark_missing_show_title(title: str, info: SizeAndViews) -> str:
  if info.views > -1:
    return title
  else:
    return f"{title}*"


def mark_missing_shows(shows: Dict[str, SizeAndViews]) -> Dict[str, SizeAndViews]:
  return {
    mark_missing_show_title(title, info): info
    for title, info in shows.items()
  }
