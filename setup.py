#!/usr/bin/env python

from setuptools import setup, find_packages


def read_requirements(path):
  with open(path, 'r') as reqs_file:
    return [line.strip() for line in reqs_file]

def read_version(path):
  with open(path, 'r') as version_file:
    return version_file.read().strip()

setup(
  name = 'plexterminator',
  version = read_version("VERSION"),
  description = 'Prints Plex show sizes and view counts to help you decide which shows to delete to free up space',
  author = 'Dan Lidral-Porter',
  author_email = 'dlp@aperiodic.org',
  url = 'ssh://git@nadia.aperiodic.org:plexterminator',

  packages = find_packages(),
  install_requires = read_requirements("requirements.txt"),
)
