# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Version 0.1.0 (No builds yet)

### Added:

The most directly useful module is a high-level read API in `plexterminator.survey`.
This contains functions in two broad classes:
  * TV surveying functions, which allow you to catalog shows by both Tautulli view counts and (estimated) size on disk;
  * Movie surveying functions, which so far only correlate movies between a Plex library and a Radarr server.
The inputs are either Plex API library sections, or config structs for Radarr/Tautulli servers (defined in `plexterminator.type_definitions`).

To service that high-level implementation, there are some minimally-viable API clients:
  * `plexterminator.tautulli` to get view history for a TV show, either raw or normalized by show or season (defaulting to season);
  * `plexterminator.radarr` to get the Radarr library and history for a particular movie.

Nothing else is worth mentioning.
This is all REPL-driven, but I did a crap job of extracting tests from REPL history so there aren't any.
But the API is designed to be really easy to use in the REPL, so test it yourself!
